#pragma once

#include <filesystem>
#include <memory>

#include <SFML/Graphics.hpp>

#include "../Toolkit/Cache.hpp"

namespace Textures {

    // Hold time elapsed since loaded
    struct AutoloadedTexture {
        explicit AutoloadedTexture(std::shared_ptr<sf::Texture> t_texture) : texture(t_texture), loaded_since() {};
        std::shared_ptr<sf::Texture> texture;
        sf::Clock loaded_since;
    };

    AutoloadedTexture load_texture_from_path(const std::filesystem::path& path);
    
    using TextureCache = Toolkit::Cache<std::filesystem::path, AutoloadedTexture, &load_texture_from_path>;
}