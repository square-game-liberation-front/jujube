rm -rf build_release
meson setup build_release --buildtype=release
meson compile -C build_release
cp ../assets assets -r
../utils/copy_dependencies.py -f jujube.exe

rm -rf jujube@exe meson* build.ninja .ninja* compile_commands.json test

cd ..
zip jujube.zip -r build
mv -t build jujube.zip
